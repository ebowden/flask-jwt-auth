import unittest
import json
import time

from project.server import db
from project.server.models import User, BlacklistToken
from project.tests.base import BaseTestCase


JOE_EMAIL = 'joe@gmail.com'
JOE_PASSWORD = '123456'


def register_user(self, email, password):
    data = json.dumps(
        dict(
            email=email,
            password=password
        )
    )
    return post(self, '/auth/register', data=data)

def login_user(self, email, password):
    data = json.dumps(
        dict(
            email=email,
            password=password
        )
    )
    return post(self, '/auth/login', data=data)

def logout_user(self, data):
    auth_token = json.loads(data.decode())['auth_token']
    headers = dict(
        Authorization='Bearer ' + auth_token
    )
    return post(self, '/auth/logout', headers=headers)

def post(self, endpoint, headers={}, data={}):
    return self.client.post(
        endpoint,
        headers=headers,
        data=data,
        content_type='application/json',
    )


class TestAuthBlueprint(BaseTestCase):

    def test_registration(self):
        """Test for user registration"""
        with self.client:
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully registered.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)

    def test_register_with_registered_user(self):
        """Test registration with already registered email"""
        user = User(
            email=JOE_EMAIL,
            password='test'
        )
        db.session.add(user)
        db.session.commit()
        with self.client:
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(
                data['message'] == 'User already exists. Please log in.')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 202)

    def test_registered_user_login(self):
        """Test login of registered user"""
        with self.client:
            # user registration
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data_register = json.loads(response.data.decode())
            self.assertTrue(data_register['status'] == 'success')
            self.assertTrue(
                data_register['message'] == 'Successfully registered.')
            self.assertTrue(data_register['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)

            # registered user login
            response = login_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged in.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)

    def test_non_registered_user_login(self):
        """Test for login of non-registered user"""
        with self.client:
            response = login_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 'User does not exist.')
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 404)

    def test_user_status(self):
        """Test user status"""
        with self.client:
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            response = self.client.get(
                '/auth/status',
                headers=dict(
                    Authorization='Bearer ' + json.loads(
                        response.data.decode()
                    )['auth_token']
                )
            )
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['data'] is not None)
            self.assertTrue(data['data']['email'] == JOE_EMAIL)
            self.assertTrue(data['data']['admin'] is 'true' or 'false')
            self.assertEqual(response.status_code, 200)

    def test_valid_logout(self):
        """Test for logout before token expires"""
        with self.client:
            # user registration
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully registered.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)

            # user login
            response = login_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged in.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)

            # valid token logout
            response = logout_user(self, response.data)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged out.')
            self.assertEqual(response.status_code, 200)

    def test_invalid_logout(self):
        """Test for logout before token expires"""
        with self.client:
            # user registration
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully registered.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)

            # user login
            response = login_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged in.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)

            # invalid token logout
            time.sleep(6)
            response = logout_user(self, response.data)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(
                data['message'] == 'Signature expired. Please log in again.')
            self.assertEqual(response.status_code, 401)

    def test_valid_blacklisted_token_logout(self):
        """Test for logout after a valid token gets blacklisted"""
        with self.client:
            # user registration
            response = register_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully registered.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 201)

            # user login
            response = login_user(self, JOE_EMAIL, JOE_PASSWORD)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertTrue(data['message'] == 'Successfully logged in.')
            self.assertTrue(data['auth_token'])
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(response.status_code, 200)

            # blacklist a valid token
            blacklist_token = BlacklistToken(
                token=json.loads(response.data.decode())['auth_token'])
            db.session.add(blacklist_token)
            db.session.commit()

            # blacklisted valid token logout
            response = logout_user(self, response.data)
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'fail')
            self.assertTrue(data['message'] == 
                'Token blacklisted. Please log in again.')
            self.assertEqual(response.status_code, 401)


if __name__ == '__main__':
    unittest.main()
